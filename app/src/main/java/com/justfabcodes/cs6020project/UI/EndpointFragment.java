package com.justfabcodes.cs6020project.UI;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.justfabcodes.cs6020project.Network.EndpointsCalculateAsyncTask;
import com.justfabcodes.cs6020project.Network.EndpointsSayHiAsyncTask;
import com.justfabcodes.cs6020project.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class EndpointFragment extends Fragment {

    @InjectView(R.id.et_say_hi)
    BootstrapEditText et_say_hi;
    @InjectView(R.id.et_1st_number)
    BootstrapEditText et_1st_number;
    @InjectView(R.id.et_2nd_number)
    BootstrapEditText et_2nd_number;
    @InjectView(R.id.ll_crouton_notification)
    LinearLayout ll_crouton_notification;

    private View view;

    public EndpointFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_endpoint, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @OnClick(R.id.bt_send_hi)
    public void onSayHiClick() {
        try {
            new EndpointsSayHiAsyncTask().execute(new Pair<Context, String>(getActivity(), et_say_hi.getText().toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.bt_send_calculation)
    public void onSendCalculationClick() {
        try{
            List<String> numbers = new ArrayList<>();
            numbers.add(et_1st_number.getText().toString());
            numbers.add(et_2nd_number.getText().toString());
            new EndpointsCalculateAsyncTask().execute(new Pair<Context, List<String>>(getActivity(), numbers));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
