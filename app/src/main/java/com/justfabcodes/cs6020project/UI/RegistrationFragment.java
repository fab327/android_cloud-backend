package com.justfabcodes.cs6020project.UI;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.justfabcodes.cs6020project.Network.GcmRegistrationAsyncTask;
import com.justfabcodes.cs6020project.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment {

    @InjectView(R.id.ll_crouton_notification)
    LinearLayout ll_crouton_notification;

    private View view;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_registration, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @OnClick(R.id.bt_registration)
    public void onStartGcmRegistrationClicked() {
        new GcmRegistrationAsyncTask(getActivity()).execute();
    }

}
