package com.justfabcodes.cs6020project.UI;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.justfabcodes.cs6020project.R;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private static FragmentManager fragmentManager;
    private static FragmentTransaction fragmentTransaction;

    private RegistrationFragment registrationFragment;
    private EndpointFragment endpointFragment;
    private AboutFragment aboutFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        if (registrationFragment == null)
            registrationFragment = new RegistrationFragment();
        if (endpointFragment == null)
            endpointFragment = new EndpointFragment();
        if (aboutFragment == null)
            aboutFragment = new AboutFragment();

        if (position == 0) {
            if (registrationFragment.isAdded()) {
                fragmentTransaction.show(registrationFragment);
            } else {
                fragmentTransaction.add(R.id.container, registrationFragment);
            }
            if (endpointFragment.isAdded())
                fragmentTransaction.hide(endpointFragment);
            if (aboutFragment.isAdded())
                fragmentTransaction.hide(aboutFragment);
        } else if (position == 1) {
            if (endpointFragment.isAdded()){
                fragmentTransaction.show(endpointFragment);
            } else {
                fragmentTransaction.add(R.id.container, endpointFragment);
            }
            if (registrationFragment.isAdded())
                fragmentTransaction.hide(registrationFragment);
            if (aboutFragment.isAdded())
                fragmentTransaction.hide(aboutFragment);
        } else if (position == 2) {
            if (aboutFragment.isAdded()) {
                fragmentTransaction.show(aboutFragment);
            } else {
                fragmentTransaction.add(R.id.container, aboutFragment);
            }
            if (registrationFragment.isAdded())
                fragmentTransaction.hide(registrationFragment);
            if (endpointFragment.isAdded())
                fragmentTransaction.hide(endpointFragment);
        }

        fragmentTransaction.commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.action_about);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_registration){
            onNavigationDrawerItemSelected(0);
            return true;
        } else if (id == R.id.action_endpoint) {
            onNavigationDrawerItemSelected(1);
            return true;
        } else if (id == R.id.action_about) {
            onNavigationDrawerItemSelected(2);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
