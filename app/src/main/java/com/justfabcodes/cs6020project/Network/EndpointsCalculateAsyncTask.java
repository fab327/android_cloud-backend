package com.justfabcodes.cs6020project.Network;

/**
 * Created by fab on 4/16/2015.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;
import android.widget.Toast;

import com.example.fab.myapplication.backend_endpoints.myApi.MyApi;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.util.List;

/**
 * Created by fab on 4/16/2015.
 */
public class EndpointsCalculateAsyncTask extends AsyncTask<Pair<Context, List<String>>, Void, String> {
    private static MyApi myApiService = null;
    private Context context;

    @Override
    protected String doInBackground(Pair<Context, List<String>>... params) {
        if (myApiService == null) {  // Only do this once
            MyApi.Builder builder = new MyApi.Builder(AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                    .setRootUrl("https://caramel-goal-91803.appspot.com/_ah/api/");
            // end options for devappserver

            myApiService = builder.build();
        }

        context = params[0].first;

        try {
            double number1 = Double.parseDouble(params[0].second.get(0));
            double number2 = Double.parseDouble(params[0].second.get(1));
            return myApiService.calculateNumbers(number1, number2).execute().getData();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(context, result, Toast.LENGTH_LONG).show();
    }
}